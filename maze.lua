-- This file is part of lua-mazes, a Lua maze-generation library
-- Copyright (C) 2017  Aidan Gauland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/>.

local lume = require "lume"

local exports = {}

local function new_cell()
  return {
    walls = {
      north = true,
      south = true,
      east = true,
      west = true
    }
  }
end

local function fresh_maze(height, width)
  local maze = {
    height = height,
    width = width
  }
  for i = 1, height do
    maze[i] = {}
    for j = 1, width do
      maze[i][j] = new_cell()
    end
  end
  return maze
end

local function all_walls_up(cell)
  return cell.walls.north and cell.walls.south and cell.walls.east and cell.walls.west
end

local function get_neighbours(maze, cell)
  local result = {}
  if cell.y > 1 then
    result.north = {y = cell.y - 1, x = cell.x}
  end
  if cell.y < maze.height then
    result.south = {y = cell.y + 1, x = cell.x}
  end
  if cell.x < maze.width then
    result.east  = {y = cell.y,     x = cell.x + 1}
  end
  if cell.x > 1 then
    result.west  = {y = cell.y,     x = cell.x - 1}
  end
  return result
end

local function knock_down_wall(maze, cell, direction)
  local neighbours = get_neighbours(maze, cell)
  local target_neighbour = neighbours[direction]

  maze[cell.y][cell.x].walls[direction] = false
  maze[target_neighbour.y][target_neighbour.x].walls[exports.reverse_direction(direction)] = false
end

function exports.reverse_direction(direction)
  if direction == "north" then
    return "south"
  elseif direction == "south" then
    return "north"
  elseif direction == "east" then
    return "west"
  elseif direction == "west" then
    return "east"
  else
    error("Invalid direction: " .. direction)
  end
end

function exports.generate_maze(height, width)
  local total_cells = height * width
  local visited_cells = 1
  local cell_stack = {}
  local maze = fresh_maze(height, width)
  local current_cell = {
    y = math.random(height),
    x = math.random(width)
  }

  while visited_cells < total_cells do
    -- Find the neighbours with all walls up.
    local neighbours = get_neighbours(maze, current_cell)
    local untouched_neighbours = {}

    for direction, coord in pairs(neighbours) do
      if all_walls_up(maze[coord.y][coord.x]) then
        table.insert(untouched_neighbours, direction)
      end
    end

    if #untouched_neighbours == 0 then
      current_cell = table.remove(cell_stack)
    else
      local next_cell_direction = lume.randomchoice(untouched_neighbours)
      local next_cell = neighbours[next_cell_direction]

      -- Knock down the wall between the two cells.
      knock_down_wall(maze, current_cell, next_cell_direction)

      table.insert(cell_stack, current_cell)
      current_cell = next_cell
      visited_cells = visited_cells + 1
    end
  end

  return maze
end

function exports.printASCII(maze)
  -- Print the top border of the maze.
  for _,_ in ipairs(maze[1]) do
    io.write(" _")
  end
  io.write("\n")

  for _,row in ipairs(maze) do
    io.write("|")
    for _,cell in ipairs(row) do
      -- Print the wall or opening to the south.
      if cell.walls.south then
        io.write("_")
      else
        io.write(" ")
      end

      -- Print the wall or opening to the east.
      if cell.walls.east then
        io.write("|")
      else
        io.write(" ")
      end
    end
    io.write("\n")
  end
end

return exports
